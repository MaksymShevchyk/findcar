const http = require('http');
const url = require('url');

const port = 8080;
const carInfoPath = '/api/v1/car-info/';
const carNumberRegExp = /^[A-ZА-Я]{2}[0-9]{4}[A-ZА-Я]{2}$/;

const dataMap = [{
	owner: 'Басова Александра Ильинична',
	modelName: 'Honda Accord IX',
	VIN: '1HGCR2F72GA224542',
	photos: [
		'https://img.automoto.ua/thumb/640-480/3/d/4/7/2/b/Honda-Accord-chernyiy-Sedan-2016-20501821.jpeg?url=aHR0cHM6Ly9pbWcwMS1vbHh1YS5ha2FtYWl6ZWQubmV0L2ltZy1vbHh1YS81OTUxNDk2MDRfMl82NDR4NDYxX2F2dG8taXotc3NoYS12LXVrcmFpbmUtaG9uZGEtYWNjb3JkLWhvbmRhLWFrb3JkLWZvdG9ncmFmaWlfcmV2MDAyLmpwZw%3D%3D',
		'https://photo.torba.com/images/Laver/c128/QEGX0QvKtxjJTdNvfTQU.jpg',
		'https://img.gh.cari.africa/2018/11/08/bW9iaWxfMzM5Nzg.jpg',
	],
	year: 2016,
	crashes: [{
		date: 1468281600,
		sourceLink: 'https://www.iaai.com/Vehicle?itemID=31324187&RowNumber=15&loadRecent=True',
	}, {
		date: 1468281600,
	}],
	owners: [{
		fullName: 'Гордеева Ангелина Сергеевна',
		period: [2016, 2017],
	}, {
		fullName: 'Басова Александра Ильинична',
		period: [2017],
	}],
}, {
	owner: 'Анкевич Павел Владимирович',
	modelName: 'LEXUS NX',
	VIN: 'JTJYARBZ6J2096400',
	photos: [
		'https://cars.ua/thumb/upload/w933/h622/q80/5a95488e0a0400_61399813.jpg',
		'',
		'',
	],
	year: 2018,
	crashes: [],
	owners: [{
		fullName: 'Анкевич Павел Владимирович',
		period: [2018],
	}],
}, {
	owner: 'Новосад Виктор Олегович',
	modelName: 'Daewoo Lanos',
	VIN: 'SUPTF696D3W548546',
	photos: [
		'https://glavnoe.ua/UserFiles/Image2016/2017/06/16/d1.jpg',
		'https://pbs.twimg.com/profile_images/378800000735338722/86b38ff710782a29338f02d462567f00.jpeg',
		'https://www.recambioverde.es/cdnx/oZ6thV1H26A=',
	],
	year: 2007,
	crashes: [{
		date: 1313971200,
	}],
	owners: [{
		fullName: 'Аллаяров Руслан Садуллаевич',
		period: [2007, 2009],
	}, {
		fullName: 'Байдин Никита Константинович',
		period: [2009, 2009],
	}, {
		fullName: 'Корчагин Никита Павлович',
		period: [2009, 2010],
	}, {
		fullName: 'Соколов Алан Георгьевич',
		period: [2010, 2010],
	}, {
		fullName: 'Кузнецов Даниил Мартынович',
		period: [2010, 2010],
	}, {
		fullName: 'Гусев Валерий Святославович',
		period: [2010, 2011],
	}, {
		fullName: 'Смирнов Владимир Агафонович',
		period: [2011, 2015],
	}, {
		fullName: 'Мамонтов Алексей Агафонович',
		period: [2015, 2015],
	}, {
		fullName: 'Демидова Анна Андреевна',
		period: [2015, 2016],
	}, {
		fullName: 'Рыхова Александра Николаевна',
		period: [2016],
	}],
}];

const server = http.createServer((request, response) => {
	const pathname = url.parse(request.url).pathname;

	response.setHeader('Access-Control-Allow-Origin', '*');

	if (pathname.startsWith(carInfoPath)) {
		const carNumber = pathname.substr(carInfoPath.length);

		// invalid request
		if (!carNumberRegExp.test(carNumber)) {
			response.statusCode = 400;
			response.setHeader('Content-Type', 'application/json');
			response.end(JSON.stringify({
				error: 'INVALID_CAR_NUMBER'
			}));
			return;
		}

		// success
		response.statusCode = 200;
		response.setHeader('Content-Type', 'application/json');
		response.end(JSON.stringify({
			result: dataMap[Math.floor(Math.random() * (dataMap.length + 1))]
		}));
		return;
	}

	// location not found
	response.statusCode = 404;
	response.end('');
});

server.listen(port, err => {
	if (err) {
		console.log('Some error has occurred:', err);
		return;
	}

	console.log(`Server is listening on ${port}`)
});
