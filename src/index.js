import "./assets/css/style.scss";

import "babel-polyfill"

import Vue from 'vue'
import App from './App.vue'
import VueResource from 'vue-resource';


import store from './store/index';

Vue.use(VueResource);

Vue.http.options.root = 'http://localhost:8080/api/v1/';

new Vue({
    el: '#app',
    store,
    render: h => h(App),
});
