import Vue from 'vue';
import Vuex from 'vuex';

Vue.use(Vuex);

const state = {
    car: null,
    toggleVisibleCar: false
};

const mutations = {
    setCar(state, result) {
        state.car = {
            ...result,
        };
    },
    toggleVisibleCar(state, type) {
        state.toggleVisibleCar = type;
    }
};

const actions = {
    async loadData({commit}, data) {
        let response;
        try {
            response = await Vue.http.get('car-info/' + data);
            const result = await response.json();
            if(!result.result) {
                commit('toggleVisibleCar',false);
                return
            }
            commit('setCar', result.result);
            commit('toggleVisibleCar',false);

        } catch (err) {
            throw err;
        }
    }
};


export default new Vuex.Store({
    state,
    mutations,
    actions,
});